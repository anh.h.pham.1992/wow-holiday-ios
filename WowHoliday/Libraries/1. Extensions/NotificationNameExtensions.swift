//
//  NotificationNameExtensions.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/8/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit
extension Notification.Name {
    static let changeLanguage = Notification.Name("changeLanguage")
}
