//
//  UIViewExtensions.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/8/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

extension UIView {
    
    static func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        // swiftlint:disable:next force_cast
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    static func loadNib() -> Self {
        return loadNib(self)
    }
    func addSubviews(_ views: UIView...) {
        for view in views {
            addSubview(view)
        }
    }
}
