//
//  StringExtensions.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/7/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import Foundation

extension String {
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
}
