//
//  ReuseableView.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/8/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var reuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
