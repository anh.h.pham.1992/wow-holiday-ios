//
//  UITableViewExtensions.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/8/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerCell<T: UITableViewCell>(type cellType: T.Type) where T: ReusableView, T: NibLoadableView {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerCell<T: UITableViewCell>(types cellTypes: [T.Type]) where T: ReusableView, T: NibLoadableView {
        for type in cellTypes {
            registerCell(type: type)
        }
    }
    
    func registerCellClass<T: UITableViewCell>(type cellType: T.Type) where T: ReusableView {
        register(cellType, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerCellClass<T: UITableViewHeaderFooterView>(type cellType: T.Type) where T: ReusableView {
        register(cellType, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerCellClass<T: UITableViewCell>(types cellTypes: [T.Type]) where T: ReusableView {
        for type in cellTypes {
            registerCellClass(type: type)
        }
    }
    
}
