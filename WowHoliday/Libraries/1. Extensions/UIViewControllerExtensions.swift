//
//  UIViewControllerExtensions.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/8/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

extension UIViewController {
    class func loadNib<T: UIViewController>(_ viewControllerType: T.Type) -> T {
        let className = String.className(viewControllerType)
        // swiftlint:disable:next force_cast
        return self.init(nibName: className, bundle: .main) as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }
}
