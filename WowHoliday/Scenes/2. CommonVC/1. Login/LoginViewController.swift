//
//  LoginViewController.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/7/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func didTappedLoginButton(_ sender: Any) {
        if #available(iOS 13.0, *) {
            guard let sceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate, let windowScene = self.view.window?.windowScene else { return }
            sceneDelegate.window = UIWindow(windowScene: windowScene)
            sceneDelegate.window?.rootViewController = MainTabBarViewController()
            sceneDelegate.window?.makeKeyAndVisible()
        } else {
            guard let appdelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            appdelegate.setupRootView(isLogin: true)
        }
        
        
    }
}
