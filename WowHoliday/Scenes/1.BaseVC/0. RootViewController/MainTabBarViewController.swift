//
//  MainTabBarViewController.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/7/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

enum TabbarItem: String {
    case home
    case myBooking
    case myAccount
    
    var image: UIImage {
        switch self {
        case .home:
            return Asset.home24Px.image
        case .myBooking:
            return Asset.receipt24Px.image
        case .myAccount:
            return Asset.accountCircle24Px.image
        }
    }
    
    var titleTabBar: String {
        switch self {
        case .home:
            return Localization.Message.ms0001
        case .myBooking:
            return Localization.Message.ms0002
        case .myAccount:
            return Localization.Message.ms0003
        }
    }
}

class MainTabBarViewController: UITabBarController {
    
    init() {
        super.init(nibName: String.className(type(of: self)), bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTabBar()

    }
    func configTabBar() {
        let homeVC = HomeViewController()
        let myBooking = UIViewController()
        let myAccount = UIViewController()

        var sequenceVCs = [UIViewController]()
        sequenceVCs = [homeVC, myBooking, myAccount]
        .map { BaseNavigationViewController(rootViewController: $0) }
        viewControllers = sequenceVCs
        setupTabbarItems()

    }
    
    func setupTabbarItems() {
        guard let tabBarItems = tabBar.items else { return }
        tabBarItems[0].title = TabbarItem.home.titleTabBar
        tabBarItems[1].title = TabbarItem.myBooking.titleTabBar
        tabBarItems[2].title = TabbarItem.myAccount.titleTabBar
        
        tabBarItems[0].image = TabbarItem.home.image
        tabBarItems[1].image = TabbarItem.myBooking.image
        tabBarItems[2].image = TabbarItem.myAccount.image
        
        self.tabBar.tintColor = ColorName.mbf2D69.color
    }
}
