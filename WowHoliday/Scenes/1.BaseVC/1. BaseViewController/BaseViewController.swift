//
//  BaseViewController.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/7/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    init() {
        super.init(nibName: String.className(type(of: self)), bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
