#!/bin/sh

#  swiftgen.sh
#  WowHoliday
#
#  Created by Huy Vuong on 10/8/19.
#  Copyright © 2019 Huy Vuong. All rights reserved.

$PODS_ROOT/SwiftGen/bin/swiftgen xcassets -t swift4 WowHoliday/Assets.xcassets -o WowHoliday/Resources/Assets.swift

$PODS_ROOT/SwiftGen/bin/swiftgen colors -t swift4 WowHoliday/Resources/colors.txt -o WowHoliday/Resources/Colors.swift

$PODS_ROOT/SwiftGen/bin/swiftgen strings -t structured-swift4 WowHoliday/en.lproj/Localizable.strings -o WowHoliday/Resources/Localization.swift

$PODS_ROOT/SwiftGen/bin/swiftgen strings --templatePath WowHoliday/Resources/SwiftGen/Templates/LocalizableStrings.stencil WowHoliday/en.lproj/Localizable.strings -o WowHoliday/Resources/Localization.swift
