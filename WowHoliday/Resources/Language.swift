//
//  Language.swift
//  WowHoliday
//
//  Created by Huy Vuong on 10/7/19.
//  Copyright © 2019 Huy Vuong. All rights reserved.
//

import UIKit

enum Language: Equatable {
    case english(English)
    case vietnamese
    
    enum English {
        case us
        case uk
    }
}

extension Language {
    
    var code: String {
        switch self {
        case .english(let english):
            switch english {
            case .us:             return "en"
            case .uk:             return "en-GB"
            }
        case .vietnamese:         return "vi"
        }
    }
    
    var name: String {
        switch self {
        case .english(let english):
            switch english {
            case .us:             return "English"
            case .uk:             return "English (UK)"
            }
        case .vietnamese:         return "Tiếng Việt"
        }
    }
}

extension Language {
    
    init?(languageCode: String?) {
        guard let languageCode = languageCode else { return nil }
        switch languageCode {
        case "en", "en-US", "en_US":     self = .english(.us)
        case "en-GB":                    self = .english(.uk)
        case "vi", "vi_VN":              self = .vietnamese
        default:                         return nil
        }
    }
}

extension Bundle {
    static func set(language: Language) {
        guard let path = Bundle.main.path(forResource: language.code, ofType: "lproj") else {
            return
        }
        Localization.bundle = Bundle(path: path)
    }
}
