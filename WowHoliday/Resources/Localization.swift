// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum Localization {

/// Update bundle if you need to change app language
static var bundle: Bundle?


  enum Message {
    /// Trang chủ
    static var ms0001: String {
      return Localization.tr("Localizable", "Message.MS0001")
    }
    /// Booking
    static var ms0002: String {
      return Localization.tr("Localizable", "Message.MS0002")
    }
    /// Account
    static var ms0003: String {
      return Localization.tr("Localizable", "Message.MS0003")
    }
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension Localization {
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: bundle ?? Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
